package eu.bcvsolutions.idm.document.repository;

import eu.bcvsolutions.idm.core.api.repository.AbstractEntityRepository;
import eu.bcvsolutions.idm.document.entity.Document;

import java.util.UUID;

/**
 * Document repository
 *
 * @author Martin Hübner
 *
 */
public interface DocumentRepository extends AbstractEntityRepository<Document> {

	void deleteAllByIdentity_Id(UUID identityId);
}
