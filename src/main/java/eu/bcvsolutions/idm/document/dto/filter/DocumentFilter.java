package eu.bcvsolutions.idm.document.dto.filter;

import eu.bcvsolutions.idm.core.api.dto.filter.DataFilter;
import eu.bcvsolutions.idm.core.api.utils.ParameterConverter;
import eu.bcvsolutions.idm.document.dto.DocumentDto;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Filter for documents
 *
 * @author Martin Hübner
 *
 */
public class DocumentFilter extends DataFilter {

	public DocumentFilter() {
		this(new LinkedMultiValueMap<>());
	}

	public DocumentFilter(MultiValueMap<String, Object> data) {
		this(data, null);
	}

	public DocumentFilter(MultiValueMap<String, Object> data, ParameterConverter parameterConverter) {
		super(DocumentDto.class, data, parameterConverter);
	}
}