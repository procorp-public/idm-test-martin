package eu.bcvsolutions.idm.document.dto;

import eu.bcvsolutions.idm.core.api.domain.DefaultFieldLengths;
import eu.bcvsolutions.idm.core.api.dto.AbstractDto;
import eu.bcvsolutions.idm.document.domain.DocumentState;
import eu.bcvsolutions.idm.document.domain.DocumentType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DocumentDto extends AbstractDto {

	@NotNull
	@Enumerated(EnumType.STRING)
	@Size(min = 0, max = 45)
	private DocumentType type;

	@NotEmpty
	@Size(min = 0, max = DefaultFieldLengths.NAME)
	private String number;

	@NotEmpty
	@Size(min = 0, max = DefaultFieldLengths.NAME)
	private String firstName;

	@NotEmpty
	@Size(min = 0, max = DefaultFieldLengths.NAME)
	private String lastName;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Size(min = 0, max = 45)
	private DocumentState state;

	public DocumentType getType() {
		return type;
	}

	public void setType(DocumentType type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public DocumentState getState() {
		return state;
	}

	public void setState(DocumentState state) {
		this.state = state;
	}
}
