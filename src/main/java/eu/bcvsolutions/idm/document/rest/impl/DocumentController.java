package eu.bcvsolutions.idm.document.rest.impl;

import eu.bcvsolutions.idm.core.api.config.swagger.SwaggerConfig;
import eu.bcvsolutions.idm.core.api.rest.AbstractReadWriteDtoController;
import eu.bcvsolutions.idm.core.api.rest.BaseController;
import eu.bcvsolutions.idm.core.security.api.domain.Enabled;
import eu.bcvsolutions.idm.document.DocumentModuleDescriptor;
import eu.bcvsolutions.idm.document.domain.DocumentGroupPermission;
import eu.bcvsolutions.idm.document.dto.DocumentDto;
import eu.bcvsolutions.idm.document.dto.filter.DocumentFilter;
import eu.bcvsolutions.idm.document.service.api.DocumentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Enabled(DocumentModuleDescriptor.MODULE_ID)
@RequestMapping(value = BaseController.BASE_PATH + "/doc")
@Api(
		value = DocumentController.TAG,
		description = "Document operations",
		tags = {DocumentController.TAG})
public class DocumentController extends AbstractReadWriteDtoController<DocumentDto, DocumentFilter> {

	protected static final String TAG = "Document";

	@Autowired
	public DocumentController(DocumentService service) {
		super(service);
	}

	@Override
	@ResponseBody
	@RequestMapping(value = "/{backendId}", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('" + DocumentGroupPermission.DOCUMENT_READ + "')")
	@ApiOperation(
			value = "Document detail",
			nickname = "getDocument",
			response = DocumentDto.class,
			tags = { DocumentController.TAG },
			authorizations = {
					@Authorization(value = SwaggerConfig.AUTHENTICATION_BASIC, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_READ, description = "") }),
					@Authorization(value = SwaggerConfig.AUTHENTICATION_CIDMST, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_READ, description = "") })
			})
	public ResponseEntity<?> get(
			@ApiParam(value = "Document's uuid identifier or code.", required = true)
			@PathVariable @NotNull String backendId) {
		return super.get(backendId);
	}

	@Override
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('" + DocumentGroupPermission.DOCUMENT_CREATE + "') or hasAuthority('" + DocumentGroupPermission.DOCUMENT_UPDATE + "')")
	@ApiOperation(
			value = "Create / update document",
			nickname = "postDocument",
			response = DocumentDto.class,
			tags = { DocumentController.TAG },
			authorizations = {
					@Authorization(value = SwaggerConfig.AUTHENTICATION_BASIC, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_CREATE, description = ""),
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_UPDATE, description = "")}),
					@Authorization(value = SwaggerConfig.AUTHENTICATION_CIDMST, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_CREATE, description = ""),
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_UPDATE, description = "")})
			})
	public ResponseEntity<?> post(@Valid @RequestBody DocumentDto dto) {
		return super.post(dto);
	}

	@Override
	@ResponseBody
	@RequestMapping(value = "/{backendId}", method = RequestMethod.PUT)
	@PreAuthorize("hasAuthority('" + DocumentGroupPermission.DOCUMENT_UPDATE + "')")
	@ApiOperation(
			value = "Update document",
			nickname = "putDocument",
			response = DocumentDto.class,
			tags = { DocumentController.TAG },
			authorizations = {
					@Authorization(value = SwaggerConfig.AUTHENTICATION_BASIC, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_UPDATE, description = "") }),
					@Authorization(value = SwaggerConfig.AUTHENTICATION_CIDMST, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_UPDATE, description = "") })
			})
	public ResponseEntity<?> put(
			@ApiParam(value = "Document's uuid identifier or code.", required = true)
			@PathVariable @NotNull String backendId,
			@Valid @RequestBody DocumentDto dto) {
		return super.put(backendId, dto);
	}

	@Override
	@ResponseBody
	@RequestMapping(value = "/{backendId}", method = RequestMethod.PATCH)
	@PreAuthorize("hasAuthority('" + DocumentGroupPermission.DOCUMENT_UPDATE + "')")
	@ApiOperation(
			value = "Update document",
			nickname = "patchDocument",
			response = DocumentDto.class,
			tags = { DocumentController.TAG },
			authorizations = {
					@Authorization(value = SwaggerConfig.AUTHENTICATION_BASIC, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_UPDATE, description = "") }),
					@Authorization(value = SwaggerConfig.AUTHENTICATION_CIDMST, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_UPDATE, description = "") })
			})
	public ResponseEntity<?> patch(
			@ApiParam(value = "Document's uuid identifier or code.", required = true)
			@PathVariable @NotNull String backendId,
			HttpServletRequest nativeRequest)
			throws HttpMessageNotReadableException {
		return super.patch(backendId, nativeRequest);
	}

	@Override
	@ResponseBody
	@RequestMapping(value = "/{backendId}", method = RequestMethod.DELETE)
	@PreAuthorize("hasAuthority('" + DocumentGroupPermission.DOCUMENT_DELETE + "')")
	@ApiOperation(
			value = "Delete document",
			nickname = "deleteDocument",
			tags = { DocumentController.TAG },
			authorizations = {
					@Authorization(value = SwaggerConfig.AUTHENTICATION_BASIC, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_DELETE, description = "") }),
					@Authorization(value = SwaggerConfig.AUTHENTICATION_CIDMST, scopes = {
							@AuthorizationScope(scope = DocumentGroupPermission.DOCUMENT_DELETE, description = "") })
			})
	public ResponseEntity<?> delete(
			@ApiParam(value = "Document's uuid identifier or code.", required = true)
			@PathVariable @NotNull String backendId) {
		return super.delete(backendId);
	}
}
