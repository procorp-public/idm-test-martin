package eu.bcvsolutions.idm.document.domain;

public enum DocumentType {
	PASSPORT,
	IDENTITY_CARD
}
