package eu.bcvsolutions.idm.document.domain;

public enum DocumentState {
	VALID,
	INVALID
}
