package eu.bcvsolutions.idm.document.event.processor;

import eu.bcvsolutions.idm.core.api.dto.IdmIdentityDto;
import eu.bcvsolutions.idm.core.api.event.CoreEvent;
import eu.bcvsolutions.idm.core.api.event.CoreEventProcessor;
import eu.bcvsolutions.idm.core.api.event.DefaultEventResult;
import eu.bcvsolutions.idm.core.api.event.EntityEvent;
import eu.bcvsolutions.idm.core.api.event.EventResult;
import eu.bcvsolutions.idm.core.api.event.processor.IdentityProcessor;
import eu.bcvsolutions.idm.core.model.event.IdentityEvent.IdentityEventType;
import eu.bcvsolutions.idm.core.security.api.domain.Enabled;
import eu.bcvsolutions.idm.document.DocumentModuleDescriptor;
import eu.bcvsolutions.idm.document.repository.DocumentRepository;

import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Component;

@Enabled(DocumentModuleDescriptor.MODULE_ID)
@Component("deleteDocumentsIdentityDeleteProcessor")
@Description("Deletes documents after identity is deleted")
public class DeleteDocumentsIdentityDeleteProcessor
		extends CoreEventProcessor<IdmIdentityDto>
		implements IdentityProcessor {
	public static final String PROCESSOR_NAME = "delete-documents-identity-delete-processor";

	private final DocumentRepository documentRepository;

	@Autowired
	public DeleteDocumentsIdentityDeleteProcessor(DocumentRepository documentRepository) {
		super(IdentityEventType.DELETE);
		//
		this.documentRepository = documentRepository;
	}

	@Override
	public EventResult<IdmIdentityDto> process(EntityEvent<IdmIdentityDto> event) {
		// event content - identity
		UUID deletedIdentityId = event.getContent().getId();
		// delete all documents
		documentRepository.deleteAllByIdentity_Id(deletedIdentityId);
		return new DefaultEventResult<>(event, this);
	}

	@Override
	public String getName() {
		return PROCESSOR_NAME;
	}

	@Override
	public int getOrder() {
		return CoreEvent.DEFAULT_ORDER + 1;
	}
}
