package eu.bcvsolutions.idm.document;

import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Initialize example module
 * 
 * @author Radek Tomiška
 * @deprecated @since 10.5.0 - unused since created
 */
@Deprecated
@Component
@DependsOn("initApplicationData")
public class DocumentModuleInitializer implements ApplicationListener<ContextRefreshedEvent> {
 
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DocumentModuleInitializer.class);
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		LOG.info("Module [{}] initialization", DocumentModuleDescriptor.MODULE_ID);
	}
}