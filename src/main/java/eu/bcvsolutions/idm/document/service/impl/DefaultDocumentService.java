package eu.bcvsolutions.idm.document.service.impl;

import eu.bcvsolutions.idm.core.api.dto.IdmIdentityDto;
import eu.bcvsolutions.idm.core.api.service.AbstractReadWriteDtoService;
import eu.bcvsolutions.idm.core.security.api.domain.BasePermission;
import eu.bcvsolutions.idm.core.security.api.service.SecurityService;
import eu.bcvsolutions.idm.document.domain.DocumentState;
import eu.bcvsolutions.idm.document.dto.DocumentDto;
import eu.bcvsolutions.idm.document.dto.filter.DocumentFilter;
import eu.bcvsolutions.idm.document.entity.Document;
import eu.bcvsolutions.idm.document.repository.DocumentRepository;
import eu.bcvsolutions.idm.document.service.api.DocumentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Default document service implementation
 *
 * @author Martin Hübner
 */
@Service("documentService")
public class DefaultDocumentService
		extends AbstractReadWriteDtoService<DocumentDto, Document, DocumentFilter>
		implements DocumentService {

	private final SecurityService securityService;

	@Autowired
	public DefaultDocumentService(DocumentRepository repository, SecurityService securityService) {
		super(repository);
		//
		this.securityService = securityService;
	}


	@Override
	public DocumentDto save(DocumentDto dto, BasePermission... permission) {
		IdmIdentityDto currentIdentity = securityService.getAuthentication().getCurrentIdentity();

		if (!dto.getFirstName().equals(currentIdentity.getFirstName()) ||
				!dto.getLastName().equals(currentIdentity.getLastName())) {
			dto.setState(DocumentState.INVALID);
		} else {
			dto.setState(DocumentState.VALID);
		}

		return super.save(dto, permission);
	}
}
