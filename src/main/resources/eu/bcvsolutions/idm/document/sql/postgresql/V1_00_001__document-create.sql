--
-- CzechIdM 7.0 Flyway script 
-- BCV solutions s.r.o.
--
-- This SQL script creates the required tables by CzechIdM (document module)
CREATE TABLE document
(
    id                   UUID                     NOT NULL,
    created              TIMESTAMP with time zone NOT NULL,
    modified             TIMESTAMP with time zone,
    creator              VARCHAR(255)             NOT NULL,
    creator_id           UUID,
    original_creator     VARCHAR(255),
    original_creator_id  UUID,
    modifier             VARCHAR(255),
    modifier_id          UUID,
    original_modifier    VARCHAR(255),
    original_modifier_id UUID,
    transaction_id       UUID,
    realm_id             UUID,
    type                 VARCHAR(45)              NOT NULL,
    number               VARCHAR(255)             NOT NULL,
    first_name           VARCHAR(255)             NOT NULL,
    last_name            VARCHAR(255)             NOT NULL,
    identity_id          UUID                     NOT NULL,
    state                VARCHAR(45)              NOT NULL,
    CONSTRAINT pk_document PRIMARY KEY (id)
);

CREATE TABLE document_aud
(
    rev                  BIGINT NOT NULL,
    revtype              SMALLINT,
    id                   UUID   NOT NULL,
    created              TIMESTAMP with time zone,
    creator              VARCHAR(255),
    creator_id           UUID,
    original_creator     VARCHAR(255),
    original_creator_id  UUID,
    modifier             VARCHAR(255),
    modifier_id          UUID,
    original_modifier    VARCHAR(255),
    original_modifier_id UUID,
    realm_id             UUID,
    type                 VARCHAR(45),
    number               VARCHAR(255),
    first_name           VARCHAR(255),
    last_name            VARCHAR(255),
    identity_id          UUID,
    state                VARCHAR(45),
    CONSTRAINT pk_document_aud PRIMARY KEY (rev, id)
);

